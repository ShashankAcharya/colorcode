<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="Automatic generation of abstract blured background based on the two or randomly chosen colors online.">
<link rel="stylesheet" type="text/css" href="design.css">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="alternate" hreflang="ru" href="https://www.imgonline.com.ua/generate-random-two-tone-blurred-background.php">
<link rel="alternate" hreflang="en" href="https://www.imgonline.com.ua/eng/generate-random-two-tone-blurred-background.php">
<title>Create abstract blurred background from two colors online - IMG online</title>
</head>
<body>
<div id="page">
<div id="logo">IMGonline.com.ua</div>
<div id="wtf">Processing of JPEG photos online.</div>
<div id="menu">
<a href="/eng/">Main page</a> | 
<a href="resize-image.php">Resize</a> | 
<a href="convert.php">Convert</a> | 
<a href="compress-image.php">Compress</a> | 
<a href="exif.php">EXIF editor</a> | 
<a href="effects.php">Effects</a> | 
<a href="photo-improvements.php">Improve</a> | 
<a href="tools.php">Different tools</a>
</div>

<div id="content">
<h1>Create abstract blurred background from two colors online</h1>

<p>Background generated automatically. Choose two colors and then click OK. Other settings are installed by default.</p>

<a href="../examples/two-tone-blurred-background-4-big.jpg"><img src="../examples/two-tone-blurred-background-4-preview.jpg" width="205" height="154" alt="Bright blue and green background"></a>
<a href="../examples/two-tone-blurred-background-5-big.jpg"><img src="../examples/two-tone-blurred-background-5-preview.jpg" width="205" height="154" alt="Dark and gloomy blured background from the two main colors"></a>
<a href="../examples/two-tone-blurred-background-6-big.jpg"><img src="../examples/two-tone-blurred-background-6-preview.jpg" width="205" height="154" alt="Red and blue abstract background"></a>

<p>If two colors will not be specified (with manual option), they will be selected from the palette randomly. You can also choose one of color - the second color will be selected randomly.</p>

<p>The serial number can be useful, for example, if you need to generate exactly the same background as last time, with the same distribution of colors, but a different size in pixels. Serial number can not be entered, it will be generated randomly and will be shown in result page after click OK.</p>

<p>Automatic creation of large two-colored background may take about 30 seconds.</p>

<div id="rtop">
</div>

<form action="generate-random-two-tone-blurred-background-result.php" method="post" enctype="multipart/form-data">

<div class="ramka">
1) <b>Generation settings</b><br><br>
The width and height in pixels:
<input type="text" name="img-width" value="1366" placeholder="от 1 до 10000" style="width:100px;"> X 
<input type="text" name="img-height" value="768" placeholder="от 1 до 10000" style="width:100px;">
(max 4000x3000 or 12 MegaPixels)
<br><br>
Serial number of background: <input type="text" name="effect-settings" value="" style="width:99px;" placeholder="случайно"> (от 1 до 999 999 999)
<br><br>
Background blurring:
<select name="effect-settings-2" size="1">
<option value="1">Maximal</option>
<option selected="selected" value="2">Normal</option>
<option value="3">Minimal</option>
</select>
<br><br>
Choice of two colors:
<input type="radio" name="effect-settings-5" value="1">Randomly
<input type="radio" name="effect-settings-5" value="2" checked="checked">Manual
<br><br>

<table width="55%">
<col width="20">
<col width="100%">
<col width="20">
<tr>
<td align="center"><b>1</b></td>
<td align="center"><b>&lt;--  Color palette to choose two colors --&gt;</b></td>
<td align="center"><b>2</b></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="1"></td>
<td bgcolor="#F08080">Light Coral</td>
<td><input type="radio" name="effect-settings-4" value="1"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="2"></td>
<td bgcolor="#CD2626">fire brick</td>
<td><input type="radio" name="effect-settings-4" value="2"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="3"></td>
<td bgcolor="#ff0000">Red</td>
<td><input type="radio" name="effect-settings-4" value="3"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="4"></td>
<td bgcolor="#FFAEB9">Light Pink</td>
<td><input type="radio" name="effect-settings-4" value="4"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="5"></td>
<td bgcolor="#FF1493">Deep Pink</td>
<td><input type="radio" name="effect-settings-4" value="5"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="6"></td>
<td bgcolor="#DA70D6">orchid</td>
<td><input type="radio" name="effect-settings-4" value="6"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="7"></td>
<td bgcolor="#FF00FF">fuchsia</td>
<td><input type="radio" name="effect-settings-4" value="7"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="8"></td>
<td bgcolor="#8B008B">Dark Magenta</td>
<td><input type="radio" name="effect-settings-4" value="8"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="9"></td>
<td bgcolor="#9400D3">Dark Violet</td>
<td><input type="radio" name="effect-settings-4" value="9"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="10"></td>
<td bgcolor="#4B0082">indigo</td>
<td><input type="radio" name="effect-settings-4" value="10"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="11"></td>
<td bgcolor="#8470FF">LightSlateBlue</td>
<td><input type="radio" name="effect-settings-4" value="11"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="12"></td>
<td bgcolor="#483D8B">Dark Slate Blue</td>
<td><input type="radio" name="effect-settings-4" value="12"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="13"></td>
<td bgcolor="#0000FF">Blue</td>
<td><input type="radio" name="effect-settings-4" value="13"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="14"></td>
<td bgcolor="#00008B">Dark Blue</td>
<td><input type="radio" name="effect-settings-4" value="14"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="15"></td>
<td bgcolor="#4169E1">Royal Blue</td>
<td><input type="radio" name="effect-settings-4" value="15"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="16"></td>
<td bgcolor="#B0C4DE">Light Steel Blue</td>
<td><input type="radio" name="effect-settings-4" value="16"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="17"></td>
<td bgcolor="#1E90FF">Dodger Blue</td>
<td><input type="radio" name="effect-settings-4" value="17"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="18"></td>
<td bgcolor="#4682B4">Steel Blue</td>
<td><input type="radio" name="effect-settings-4" value="18"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="19"></td>
<td bgcolor="#87CEFF">Sky Blue</td>
<td><input type="radio" name="effect-settings-4" value="19"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="20"></td>
<td bgcolor="#00F5FF">turquoise</td>
<td><input type="radio" name="effect-settings-4" value="20"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="21"></td>
<td bgcolor="#00CED1">Dark Turquoise</td>
<td><input type="radio" name="effect-settings-4" value="21"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="22"></td>
<td bgcolor="#E0FFFF">Light Cyan</td>
<td><input type="radio" name="effect-settings-4" value="22"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="23"></td>
<td bgcolor="#7FFFD4">aquamarine</td>
<td><input type="radio" name="effect-settings-4" value="23"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="24"></td>
<td bgcolor="#00FF7F">Spring Green</td>
<td><input type="radio" name="effect-settings-4" value="24"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="25"></td>
<td bgcolor="#2E8B57">Sea Green</td>
<td><input type="radio" name="effect-settings-4" value="25"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="26"></td>
<td bgcolor="#F0FFF0">honey dew</td>
<td><input type="radio" name="effect-settings-4" value="26"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="27"></td>
<td bgcolor="#9AFF9A">Pale Green</td>
<td><input type="radio" name="effect-settings-4" value="27"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="28"></td>
<td bgcolor="#00FF00">lime</td>
<td><input type="radio" name="effect-settings-4" value="28"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="29"></td>
<td bgcolor="#228B22">Forest Green</td>
<td><input type="radio" name="effect-settings-4" value="29"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="30"></td>
<td bgcolor="#006400">Dark Green</td>
<td><input type="radio" name="effect-settings-4" value="30"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="31"></td>
<td bgcolor="#7CFC00">Lawn Green</td>
<td><input type="radio" name="effect-settings-4" value="31"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="32"></td>
<td bgcolor="#9ACD32">Olive Drab</td>
<td><input type="radio" name="effect-settings-4" value="32"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="33"></td>
<td bgcolor="#FFFFF0">ivory</td>
<td><input type="radio" name="effect-settings-4" value="33"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="34"></td>
<td bgcolor="#F5F5DC">beige</td>
<td><input type="radio" name="effect-settings-4" value="34"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="35"></td>
<td bgcolor="#808000">olive</td>
<td><input type="radio" name="effect-settings-4" value="35"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="36"></td>
<td bgcolor="#BDB76B">Dark Khaki</td>
<td><input type="radio" name="effect-settings-4" value="36"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="37"></td>
<td bgcolor="#FFD700">gold</td>
<td><input type="radio" name="effect-settings-4" value="37"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="38"></td>
<td bgcolor="#FFFF00">yellow</td>
<td><input type="radio" name="effect-settings-4" value="38"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="39"></td>
<td bgcolor="#FFEC8B">Light Goldenrod</td>
<td><input type="radio" name="effect-settings-4" value="39"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="40"></td>
<td bgcolor="#CD950C">Dark Goldenrod</td>
<td><input type="radio" name="effect-settings-4" value="40"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="41"></td>
<td bgcolor="#FFFAF0">Floral White</td>
<td><input type="radio" name="effect-settings-4" value="41"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="42"></td>
<td bgcolor="#F5DEB3">wheat</td>
<td><input type="radio" name="effect-settings-4" value="42"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="43"></td>
<td bgcolor="#FFA500">orange</td>
<td><input type="radio" name="effect-settings-4" value="43"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="44"></td>
<td bgcolor="#FFEBCD">Blanched Almond</td>
<td><input type="radio" name="effect-settings-4" value="44"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="45"></td>
<td bgcolor="#FFDEAD">Navajo White</td>
<td><input type="radio" name="effect-settings-4" value="45"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="46"></td>
<td bgcolor="#FAEBD7">AntiqueWhite</td>
<td><input type="radio" name="effect-settings-4" value="46"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="47"></td>
<td bgcolor="#D2691E">chocolate</td>
<td><input type="radio" name="effect-settings-4" value="47"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="48"></td>
<td bgcolor="#8B4513">brown chocolate</td>
<td><input type="radio" name="effect-settings-4" value="48"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="49"></td>
<td bgcolor="#FFF5EE">seashell</td>
<td><input type="radio" name="effect-settings-4" value="49"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="50"></td>
<td bgcolor="#FF8247">sienna</td>
<td><input type="radio" name="effect-settings-4" value="50"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="51"></td>
<td bgcolor="#FA8072">salmon</td>
<td><input type="radio" name="effect-settings-4" value="51"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="52"></td>
<td bgcolor="#FFFFFF">White</td>
<td><input type="radio" name="effect-settings-4" value="52"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="53"></td>
<td bgcolor="#000000"><span style="color: white">Black</span></td>
<td><input type="radio" name="effect-settings-4" value="53"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="54"></td>
<td bgcolor="#696969">Gray</td>
<td><input type="radio" name="effect-settings-4" value="54"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="55"></td>
<td bgcolor="#C0C0C0">silver</td>
<td><input type="radio" name="effect-settings-4" value="55"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="56"></td>
<td bgcolor="#D3D3D3">Light Gray</td>
<td><input type="radio" name="effect-settings-4" value="56"></td>
</tr>
<tr>
<td><input type="radio" name="effect-settings-3" value="57"></td>
<td bgcolor="#F5F5F5">White Smoke</td>
<td><input type="radio" name="effect-settings-4" value="57"></td>
</tr>
</table>
</div>

<div class="ramka">
2) Compression settings<br><br>
<input type="radio" name="jpeg-conv-type" value="1" checked="checked">Standard JPEG
<input type="radio" name="jpeg-conv-type" value="2">Progressive JPEG<br><br>
Quality (1-100) <input type="text" name="jpeg-quality" value="92" style="width:60px;"><br>
</div>

<div class="ramka">
<input type="submit" value="OK" style="width:250px; height:40px; font-size:20px; text-align:center;">
</div>
</form>

<div id="rbottom">
<!-- Ukrainian Banner Network 468x60 START --><center><script type='text/javascript'>var _ubn=_ubn||{sid:Math.round((Math.random()*10000000)),data:[]};(function(){var n=document.getElementsByTagName('script');_ubn.data.push({user: 109730, format_id: 1, page: 1,pid: Math.round((Math.random()*10000000)),placeholder: n[n.length-1]});if(!_ubn.code)(function() {var script = document.createElement('script');script.type = 'text/javascript'; _ubn.code= script.async = script.defer = true;script.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'banner.kiev.ua/j/banner.js?'+_ubn.sid;n[0].parentNode.insertBefore(script,n[0]);})();})();</script></center><!-- Ukrainian Banner Network 468x60 END --></div>

</div>

<div id="pf"><a href="contact.php">Contact</a> | <a href="sitemap.php">Site map, limitations</a> | <a href="../generate-random-two-tone-blurred-background.php">Ukrainian version</a></div>
<div id="co">&copy; 2018 www.imgonline.com.ua</div>
</div>
</body>
</html>
