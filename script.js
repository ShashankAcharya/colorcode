function reset(){
    document.getElementById("hex").value = '';
    document.getElementById("r").value = '';
    document.getElementById("g").value = '';
    document.getElementById("b").value = '';
    document.getElementById("h").value = '';
    document.getElementById("s").value = '';
    document.getElementById("l").value = '';
    document.getElementById("preview").style.backgroundColor = 'white';
}
function hexa(){
    var hexavalue = document.getElementById('hex').value;
    document.getElementById('preview').style.backgroundColor = hexavalue;
}
function rgb(){
    var red = parseInt(document.getElementById('r').value);
    var green = parseInt(document.getElementById('g').value);
    var blue = parseInt(document.getElementById('b').value);
    var rgbvalue = "rgb("+ red + "," + green + "," + blue + ")";
    rgbvalue = String(rgbvalue);
    document.getElementById('preview').style.backgroundColor = rgbvalue;
}
function hsl(){
    var hue = parseInt(document.getElementById('h').value);
    var satu = parseInt(document.getElementById('s').value);
    var lumi = parseInt(document.getElementById('l').value);
    var hslvalue = "hsl("+ hue + "," + satu + "%," + lumi + "%)";
    hslvalue = String(hslvalue);
    document.getElementById('preview').style.backgroundColor = hslvalue;
}